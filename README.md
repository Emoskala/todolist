## Installation

To install dependencies use:

### `npm install`

## Running

Before starting app you have to make sure your connection to MySQL server is configured.
In server folder is file named 'db.js', you have to enter it and you can configure your connection to MySQL server.

To run app use first on server, then on client:

### `npm start`

## Testing

To run tests use:

### `npm test`
