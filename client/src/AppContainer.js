import App from "./App";
import { Provider } from "react-redux";
import { store } from "store";
import "importStyles";

const AppContainer = () => {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    );
};

export default AppContainer;
