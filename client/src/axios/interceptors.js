import jwt_decode from "jwt-decode";
import { refreshAccessToken } from "./userAPI";

export const handleTokenExpirationTime = async (req) => {
    const refreshToken = localStorage.getItem("refreshToken");
    const accessToken = localStorage.getItem("accessToken");

    if (!(accessToken && jwt_decode(accessToken).exp * 1000 >= Date.now()) && !(refreshToken && jwt_decode(refreshToken).exp * 1000 >= Date.now())) {
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("accessToken");
        window.location.reload();
    } else if (!(accessToken && jwt_decode(accessToken).exp * 1000 >= Date.now()) && refreshToken && jwt_decode(refreshToken).exp * 1000 >= Date.now()) {
        await refreshAccessToken()
            .then((res) => {
                const { error } = res.data;
                if (error) {
                    window.location.reload();
                }
            })
            .catch((err) => console.log(err));
    }
    return req;
};

export const handleAddToken = (req) => {
    switch (req.method) {
        case "get":
            req.params.accessToken = localStorage.getItem("accessToken");
            break;
        default:
            req.data.accessToken = localStorage.getItem("accessToken");
            break;
    }
    return req;
};

export const handleFail = (error) => {
    throw new Error(error);
};

export const toDoListInterceptor = async (req) => {
    req = await handleTokenExpirationTime(req);
    req = handleAddToken(req);
    return req;
};
