import axios from "axios";

const instance = axios.create({
    baseURL: "http://localhost:5500/user",
    headers: {
        "Content-Type": "application/json",
    },
});

export const signIn = (username, password) => {
    return instance({
        method: "POST",
        url: "/sign-in",
        data: {
            username,
            password,
        },
    })
        .then(handleResponse)
        .catch((err) => err.response);
};

export const register = (username, password) => {
    return instance({
        method: "POST",
        url: "/register",
        data: {
            username,
            password,
        },
    })
        .then(handleResponse)
        .catch((err) => err.response);
};

export const refreshAccessToken = () => {
    return instance({
        method: "POST",
        url: "/refresh-access-token",
        data: { refreshToken: localStorage.getItem("refreshToken") },
    })
        .then(handleResponse)
        .catch((err) => err.response);
};

/**
 * Common function for every response.
 */
const handleResponse = (res) => {
    const { accessToken, refreshToken } = res.data.results;

    accessToken && localStorage.setItem("accessToken", accessToken);
    refreshToken && localStorage.setItem("refreshToken", refreshToken);

    return res;
};
