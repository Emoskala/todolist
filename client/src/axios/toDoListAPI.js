import axios from "axios";
import { toDoListInterceptor, handleFail } from "./interceptors";

const instance = axios.create({
    baseURL: "http://localhost:5500/to-do-list",
    headers: {
        "Content-Type": "application/json",
    },
});

instance.interceptors.request.use(toDoListInterceptor, handleFail);

export const getTasks = () => {
    return instance({
        method: "GET",
        url: "get-tasks",
        params: {},
    }).catch((err) => err.response);
};

export const deleteTask = (taskId) => {
    return instance({
        method: "DELETE",
        url: "/delete-task",
        data: {
            taskId,
        },
    }).catch((err) => err.response);
};

export const editTask = (taskId, content) => {
    return instance({
        method: "PATCH",
        url: "/edit-task",
        data: {
            taskId,
            content,
        },
    }).catch((err) => err.response);
};
export const createTask = (content) => {
    return instance({
        method: "POST",
        url: "/create-task",
        data: {
            content,
        },
    }).catch((err) => err.response);
};
