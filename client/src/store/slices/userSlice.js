import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: "user",
    initialState: {
        username: null,
        pending: false,
        error: false,
    },
    reducers: {
        logout: (state) => {
            return {
                ...state,
                username: null,
            };
        },
        loginSuccess: (state, action) => {
            return {
                ...state,
                username: action.payload.username,
                pending: false,
                error: false,
            };
        },

        userPending: (state) => {
            return {
                ...state,
                pending: true,
            };
        },

        userError: (state, action) => {
            return {
                ...state,
                pending: false,
                error: action.payload,
            };
        },

        removeError: (state) => {
            return {
                ...state,
                error: false,
            };
        },
    },
});
