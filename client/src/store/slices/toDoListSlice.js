import { createSlice } from "@reduxjs/toolkit";

export const toDoListSlice = createSlice({
    name: "toDoList",
    initialState: {
        tasksList: null,
        pending: false,
        error: false,
    },
    reducers: {
        toDoListPending: (state, action) => {
            return {
                ...state,
                pending: true,
            };
        },

        toDoListSuccess: (state, action) => {
            return {
                ...state,
                pending: false,
                tasksList: action.payload,
            };
        },

        toDoListError: (state, action) => {
            return {
                ...state,
                pending: false,
                error: action.payload,
            };
        },

        removeError: (state) => {
            return {
                ...state,
                error: false,
            };
        },
    },
});
