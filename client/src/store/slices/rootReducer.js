import { combineReducers } from "redux";
import { userSlice } from "./userSlice";
import { toDoListSlice } from "./toDoListSlice";

export const rootReducer = combineReducers({ user: userSlice.reducer, toDoList: toDoListSlice.reducer });
