export const getTasks = (state) => {
    return state.toDoList.tasksList;
};
export const getError = (state) => {
    return state.toDoList.error;
};
