export const getUsername = (state) => {
    return state.user.username;
};

export const getError = (state) => {
    return state.user.error;
};
