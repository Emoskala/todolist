import { toDoListSlice } from "store/slices/toDoListSlice";
import { getTasks as getTasksRequest, createTask as createTaskRequest, editTask as editTaskRequest, deleteTask as deleteTaskRequest } from "axios/toDoListAPI";

export const getTasks = () => {
    return async (dispatch) => {
        handleResponse(dispatch, getTasksRequest);
    };
};

export const createTask = (content) => {
    return async (dispatch) => {
        handleResponse(dispatch, createTaskRequest, content);
    };
};

export const editTask = (taskId, content) => {
    return async (dispatch) => {
        handleResponse(dispatch, editTaskRequest, taskId, content);
    };
};
export const deleteTask = (taskId) => {
    return async (dispatch) => {
        handleResponse(dispatch, deleteTaskRequest, taskId);
    };
};

/**
 * Common function to handle response
 */
const handleResponse = async (dispatch, req, ...args) => {
    dispatch(toDoListSlice.actions.toDoListPending());

    const res = await req(...args);

    const { error, message, results } = res.data;

    if (!error) {
        dispatch(toDoListSlice.actions.toDoListSuccess(results));
    } else {
        dispatch(toDoListSlice.actions.toDoListError(message));
    }
};
