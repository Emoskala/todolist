import { signIn as signInRequest, register as registerRequest, refreshAccessToken as refreshAccessTokenRequest } from "axios/userAPI";
import { userSlice } from "store/slices/userSlice";

export const signIn = (login, password) => {
    return async (dispatch) => {
        handleResponse(dispatch, signInRequest, login, password);
    };
};

export const register = (login, password) => {
    return async (dispatch) => {
        handleResponse(dispatch, registerRequest, login, password);
    };
};

export const refreshAccessToken = () => {
    return async (dispatch) => {
        dispatch(userSlice.actions.userPending());

        const res = await refreshAccessTokenRequest();

        const { error, results, message } = res.data;

        if (!error) {
            dispatch(userSlice.actions.loginSuccess({ username: results.username }));
        } else {
            dispatch(userSlice.actions.userError(message));
            localStorage.removeItem("accessToken");
            localStorage.removeItem("refreshToken");
        }
    };
};

/**
 * Common function to handle response
 */
const handleResponse = async (dispatch, req, ...args) => {
    dispatch(userSlice.actions.userPending());

    const res = await req(...args);

    const { error, results, message } = res.data;

    if (!error) {
        dispatch(userSlice.actions.loginSuccess({ username: results.username }));
    } else {
        dispatch(userSlice.actions.userError(message));
    }
};
