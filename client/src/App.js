import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUsername } from "store/selectors/user";
import { refreshAccessToken } from "store/middlewares/user";
import Login from "components/Login";
import ToDoList from "components/ToDoList";
import { Error } from "components/Error";
import { Loader } from "components/Loader";

export const App = () => {
    const dispatch = useDispatch();
    const username = useSelector(getUsername);

    /**
     * handle user at render.
     */
    useEffect(() => {
        const refreshToken = localStorage.getItem("refreshToken");
        refreshToken && dispatch(refreshAccessToken());
    }, []);

    return (
        <div className="app">
            {!username && <Login />}
            {username && <ToDoList />}
            <Error />
            <Loader />
        </div>
    );
};

export default App;
