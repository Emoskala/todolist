import { useSelector } from "react-redux";
import { getTasks } from "store/selectors/toDoList";
import { Task } from "./Task";

export const TasksList = () => {
    const tasks = useSelector(getTasks);

    const createTasks = () => {
        return (
            tasks &&
            tasks
                .map((task, index) => {
                    return <Task content={task.content} id={task.id} key={index} />;
                })
                .reverse()
        );
    };

    return <ul className="to-do-list__list">{createTasks()}</ul>;
};
