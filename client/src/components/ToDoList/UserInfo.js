import { useSelector } from "react-redux";
import { getUsername } from "store/selectors/user";

export const UserInfo = () => {
    const username = useSelector(getUsername);

    return <span>Username: {username}</span>;
};
