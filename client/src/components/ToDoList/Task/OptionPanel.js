import { DeleteTaskButton } from "./DeleteTaskButton";
import { EditTaskButton } from "./EditTaskButton";

export const OptionPanel = ({ id, setEdit, edit }) => {
    return (
        <div className="to-do-list__task-option-panel">
            <EditTaskButton setEdit={setEdit} edit={edit} />
            <DeleteTaskButton id={id} />
        </div>
    );
};
