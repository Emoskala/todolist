import { EditTaskContent } from "./EditTaskContent";

export const TaskContent = ({ content, edit, setEdit, id }) => {
    return (
        <div className="to-do-list__task-content">
            {!edit && <p className="to-do-list__task-content-text">{content}</p>}
            {edit && <EditTaskContent content={content} id={id} setEdit={setEdit} />}
        </div>
    );
};
