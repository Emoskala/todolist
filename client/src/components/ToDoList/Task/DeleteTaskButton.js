import { deleteTask } from "store/middlewares/toDoList";
import { useDispatch } from "react-redux";
import DeleteIcon from "@material-ui/icons/Delete";
import { IconButton } from "@material-ui/core";

export const DeleteTaskButton = ({ id }) => {
    const dispatch = useDispatch();

    const handleDelete = () => {
        dispatch(deleteTask(id));
    };

    return (
        <IconButton onClick={handleDelete}>
            <DeleteIcon />
        </IconButton>
    );
};
