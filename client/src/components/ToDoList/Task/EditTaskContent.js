import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { editTask } from "store/middlewares/toDoList";
import { Input } from "@material-ui/core";
import { SubmitTaskButton } from "./SubmitTaskButton";

export const EditTaskContent = ({ id, content, setEdit }) => {
    const dispatch = useDispatch();
    const [value, setValue] = useState("");

    useEffect(() => {
        setValue(content);
    }, [content]);

    const handleEditSubmit = () => {
        dispatch(editTask(id, value));
        setEdit(false);
    };

    const handleValueChange = (event) => {
        setValue(event.currentTarget.value);
    };

    return (
        <>
            <Input type="text" onChange={handleValueChange} value={value} className="to-do-list__edit-input" />
            <SubmitTaskButton onClick={handleEditSubmit} />
        </>
    );
};
