import { useState } from "react";
import { TaskContent } from "./TaskContent";
import { OptionPanel } from "./OptionPanel";

export const Task = ({ content, id }) => {
    const [edit, setEdit] = useState(false);

    return (
        <li className="to-do-list__task">
            <TaskContent edit={edit} content={content} setEdit={setEdit} id={id} />
            <OptionPanel setEdit={setEdit} edit={edit} id={id} />
        </li>
    );
};
