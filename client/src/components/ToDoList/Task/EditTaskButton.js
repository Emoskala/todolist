import EditIcon from "@material-ui/icons/Edit";
import ClearIcon from "@material-ui/icons/Clear";
import { IconButton } from "@material-ui/core";

export const EditTaskButton = ({ setEdit, edit }) => {
    const handleClick = () => {
        setEdit(!edit);
    };

    return (
        <>
            {!edit && (
                <IconButton onClick={handleClick}>
                    <EditIcon />
                </IconButton>
            )}
            {edit && (
                <IconButton onClick={handleClick}>
                    <ClearIcon />
                </IconButton>
            )}
        </>
    );
};
