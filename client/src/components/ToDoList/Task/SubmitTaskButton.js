import { IconButton } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";

export const SubmitTaskButton = ({ onClick }) => {
    return (
        <IconButton onClick={onClick}>
            <DoneIcon />
        </IconButton>
    );
};
