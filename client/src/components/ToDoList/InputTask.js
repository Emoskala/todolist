import { useState } from "react";
import { useDispatch } from "react-redux";
import { createTask } from "store/middlewares/toDoList";
import { Input, Button } from "@material-ui/core";

export const InputTask = () => {
    const [value, setValue] = useState("");
    const dispatch = useDispatch();

    const changeValue = (event) => {
        setValue(event.currentTarget.value);
    };

    const handleAddTask = () => {
        value && dispatch(createTask(value)) && setValue("");
    };

    return (
        <div className="to-do-list__input-task-container">
            <Input type="text" value={value} onChange={changeValue} className="to-do-list__input-task" />
            <Button onClick={handleAddTask}>add</Button>
        </div>
    );
};
