import { useDispatch } from "react-redux";
import { UserInfo } from "./UserInfo";
import { userSlice } from "store/slices/userSlice";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import IconButton from "@material-ui/core/IconButton";

export const UserPanel = () => {
    const dispatch = useDispatch();

    const handleLogout = () => {
        dispatch(userSlice.actions.logout());
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
    };

    return (
        <div className="user-panel">
            <div className="user-panel__user-info">
                <UserInfo />
            </div>
            <div className="user-panel__logout-button">
                <IconButton color="inherit" onClick={handleLogout}>
                    <ExitToAppIcon />
                </IconButton>
            </div>
        </div>
    );
};
