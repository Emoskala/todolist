import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { TasksList } from "./TasksList";
import { InputTask } from "./InputTask";
import { getTasks } from "store/middlewares/toDoList";
import { UserPanel } from "./UserPanel";

export const ToDoList = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getTasks());
    }, []);

    return (
        <div className="to-do-list">
            <InputTask />
            <TasksList />
            <UserPanel />
        </div>
    );
};

export default ToDoList;
