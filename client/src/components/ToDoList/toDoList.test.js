import { render } from "@testing-library/react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import { unmountComponentAtNode } from "react-dom";
import { TasksList } from "./TasksList";

const mockStore = configureMockStore();

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

test("Tasks list is rendered correctly", () => {
    const store = mockStore({
        toDoList: {
            tasksList: [
                { id: 0, content: "Task" },
                { id: 1, content: "Task" },
                { id: 2, content: "Task" },
            ],
        },
    });

    const wrapper = render(
        <Provider store={store}>
            <TasksList />
        </Provider>
    );

    expect(wrapper.getAllByText(/Task/).length).toBe(3);
});
