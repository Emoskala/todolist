import { LoaderPage } from "./LoaderPage";
import { useSelector } from "react-redux";

/**
 * Creates Loader component which handles pending requests.
 */
export const Loader = () => {
    const state = useSelector((state) => state);

    return (
        <>
            {state.user.pending && <LoaderPage />}
            {state.toDoList.pending && <LoaderPage />}
        </>
    );
};
