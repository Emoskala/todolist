import { useDispatch } from "react-redux";
import { userSlice } from "store/slices/userSlice";
import { toDoListSlice } from "store/slices/toDoListSlice";
import ErrorIcon from "@material-ui/icons/Error";
import CloseIcon from "@material-ui/icons/Close";

export const ErrorPage = ({ errorMessage, errorSource }) => {
    const dispatch = useDispatch();

    /**
     * Disables error message.
     */
    const handleDisableError = () => {
        errorSource === "toDoList" && dispatch(toDoListSlice.actions.removeError());
        errorSource === "user" && dispatch(userSlice.actions.removeError());
    };

    return (
        <div className="error-block">
            <div className="error">
                <div className="error__icon">
                    <ErrorIcon fontSize="inherit" />
                </div>
                <div className="error__message-block">
                    <p className="error__message">{errorMessage}</p>
                </div>
                <div className="error__close-button">
                    <button onClick={handleDisableError} className="error__close-button">
                        <CloseIcon />
                    </button>
                </div>
            </div>
        </div>
    );
};
