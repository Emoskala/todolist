import { ErrorPage } from "./ErrorPage";
import { useSelector } from "react-redux";
import { getError as getUserError } from "store/selectors/user";
import { getError as getToDoListError } from "store/selectors/toDoList";

/**
 * Creates Error component which handles errors.
 */
export const Error = () => {
    const userError = useSelector(getUserError);
    const toDoListError = useSelector(getToDoListError);

    return (
        <>
            {userError && <ErrorPage errorMessage={userError} errorSource={"user"} />}
            {toDoListError && <ErrorPage errorMessage={toDoListError} errorSource={"toDoList"} />}
        </>
    );
};
