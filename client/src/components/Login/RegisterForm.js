import { register } from "store/middlewares/user";
import { Form } from "./Form";

export const RegisterForm = () => {
    return <Form submitMethod={register} title="Register!" />;
};
