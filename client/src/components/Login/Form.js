import { useDispatch } from "react-redux";
import { useFields } from "./useFields";
import { Field } from "./Field";
import { Button } from "@material-ui/core";

export const Form = ({ submitMethod, title }) => {
    const [values, setValue, setValues] = useFields({ username: "", password: "" });

    const dispatch = useDispatch();

    const onSubmit = (event) => {
        event.preventDefault();
        const { username, password } = values;
        username && password && dispatch(submitMethod(username, password));
    };

    return (
        <div className="login__form-container">
            <h1 className="login__title">{title}</h1>
            <form action="submit" onSubmit={onSubmit} className="login__form">
                <Field name="username" type="text" value={values.username} setValue={setValue} />
                <Field name="password" type="password" value={values.password} setValue={setValue} />
                <Button type="submit">Submit</Button>
            </form>
        </div>
    );
};
