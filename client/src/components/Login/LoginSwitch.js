import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { SignInForm } from "./SignInForm";
import { RegisterForm } from "./RegisterForm";

export const LoginSwitch = ({}) => {
    return (
        <Switch>
            <Route exact path="/signin">
                <SignInForm />
            </Route>
            <Route exact path="/register">
                <RegisterForm />
            </Route>
            <Route path="/">
                <Redirect to="/signin" />
            </Route>
        </Switch>
    );
};
