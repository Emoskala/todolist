import { signIn } from "store/middlewares/user";
import { Form } from "./Form";

export const SignInForm = () => {
    return <Form submitMethod={signIn} title="Sign in!" />;
};
