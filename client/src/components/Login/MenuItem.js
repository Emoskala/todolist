import React, { useState } from "react";
import { Link } from "react-router-dom";

export const MenuItem = ({ linkTo, children, active, blockClassName }) => {
    return (
        <li className={`${blockClassName}__menu-item ${active ? blockClassName + "__menu-item--active" : ""}`}>
            <Link className={`${blockClassName}__link`} to={linkTo}>
                {children}
            </Link>
        </li>
    );
};
