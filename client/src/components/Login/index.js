import { BrowserRouter as Router } from "react-router-dom";
import { LoginMenu } from "./LoginMenu";
import { LoginSwitch } from "./LoginSwitch";

export const Login = () => {
    return (
        <div className="login">
            <Router>
                <LoginMenu />
                <LoginSwitch />
            </Router>
        </div>
    );
};

export default Login;
