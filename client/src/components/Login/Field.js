import { Input } from "@material-ui/core";

export const Field = ({ name, type, value, setValue }) => {
    const handleChange = (event) => {
        setValue(name, event.currentTarget.value);
    };

    return (
        <Input
            autoComplete="off"
            type={type}
            name={name}
            className="login__form-input"
            value={value}
            onChange={handleChange}
            placeholder={`Enter ${name} here`}
        />
    );
};
