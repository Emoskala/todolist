import React from "react";
import { useLocation } from "react-router-dom";
import { MenuItem } from "./MenuItem";

export const LoginMenu = ({}) => {
    const location = useLocation();

    return (
        <nav className="login__menu">
            <ul className="login__list">
                <MenuItem active={location.pathname === "/signin"} linkTo="/signin" blockClassName="login">
                    Sign in
                </MenuItem>
                <MenuItem active={location.pathname === "/register"} linkTo="/register" blockClassName="login">
                    Register
                </MenuItem>
            </ul>
        </nav>
    );
};
