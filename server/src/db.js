const mysql = require("mysql");

//Config for MySQL server connection
const connectionConfig = {
    host: "localhost",
    user: "root",
    password: "root",
    multipleStatements: true,
};

let _db = null;

const initDB = () => {
    if (_db) {
        console.warn("You're Trying to connect DB again!");
        return _db;
    } else {
        _db = mysql.createConnection(connectionConfig);

        _db.connect(function (err) {
            if (err) {
                console.error("error connecting: " + err.stack);
                return;
            }
            _db.query(sqlCommand);

            console.log("connected as id " + _db.threadId);
        });
    }
};

const getDB = () => {
    if (_db) return _db;
    else console.warn("You have to init connection first!");
};

module.exports = { initDB, getDB };

const sqlCommand = `
CREATE DATABASE IF NOT EXISTS todolist CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE todolist;

CREATE TABLE IF NOT EXISTS user (
	id INT auto_increment NOT NULL,
	username varchar(20) UNIQUE NOT NULL,
	password varchar(20) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS task (
	id INT auto_increment NOT NULL,
	content varchar(80) NOT NULL,
	username varchar(20) NOT NULL ,
    PRIMARY KEY (id),
    FOREIGN KEY (username) REFERENCES user (username)
);
INSERT IGNORE INTO user (username, password) VALUES ('testuser', 'testuser')
`;
