const { getDB } = require("./db");
const request = require("supertest");
const app = require("./app");

beforeAll(async (done) => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    done();
});

test("Should sign in existing user with correct username and password ", async () => {
    const response = await request(app)
        .post("/user/sign-in")
        .send({ username: "testuser", password: "testuser" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200);

    expect(response.body.error).toBeFalsy();
    expect(response.body.results.accessToken).toBeTruthy();
    expect(response.body.results.refreshToken).toBeTruthy();
    expect(response.body.results.username).toBeTruthy();
});

test("Should not sign in not existing user", async () => {
    const response = await request(app)
        .post("/user/sign-in")
        .send({ username: "notExists", password: "notExists" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(404);

    expect(response.body.error).toBeTruthy();
});

test("Should not be able to get tasks without valid accessToken", async () => {
    const response = await request(app)
        .get("/to-do-list/get-tasks")
        .query({ accessToken: "notValidToken" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(401);

    expect(response.body.error).toBeTruthy();
});
