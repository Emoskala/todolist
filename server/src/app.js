const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const auth = require("./Domains/Token/auth.middleware");
const getUsernameFromToken = require("./Domains/Token/getUsernameFromToken.middleware");
const { initDB } = require("./db");

// Create express app
const app = express();

// Make connection to DB.
initDB();

// Get required .env variables.
const dotenv = require("dotenv");
dotenv.config({ path: "./.env" });
const PORT = process.env.PORT || 6000;

// Middlewares
app.use(cors());
app.use(morgan("common"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/to-do-list", auth());
app.use(getUsernameFromToken);

// Routes
const user = require("./Domains/User/user.controller");
const task = require("./Domains/Task/task.controller");
app.use("/user", user);
app.use("/to-do-list", task);

// Error handler
const { error } = require("./responseApi");

app.use((err, req, res, next) => {
    const { name, status, message } = err;

    if (name === "UnauthorizedError") {
        res.status(401).send(error(401, "Invalid Token!"));
    } else {
        res.status(status).send(error(status, message));
    }
});

// Set app to listen at port.
app.listen(PORT, () => {
    console.log("Server is listening at port " + PORT);
});

module.exports = app;
