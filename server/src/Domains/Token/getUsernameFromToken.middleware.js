const { decodeToken } = require("./token.service");

module.exports = (req, res, next) => {
    if (req.body.accessToken) req.body.username = decodeToken(req.body.accessToken).username;
    if (req.query.accessToken) req.query.username = decodeToken(req.query.accessToken).username;
    next();
};
