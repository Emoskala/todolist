const expressJwt = require("express-jwt");
const dotenv = require("dotenv");
dotenv.config({ path: "./.env" });
const JWT_AT_SECRET_KEY = process.env.JWT_AT_SECRET_KEY;

/**
 * Verifies delivered access token.
 */
module.exports = () => {
    return expressJwt({ secret: JWT_AT_SECRET_KEY, algorithms: ["HS256"], isRevoked, getToken });
};

/**
 * Returns a token from request.
 */
const getToken = (req) => {
    return req.query.accessToken || req.body.accessToken;
};

const isRevoked = (req, payload, done) => {
    done();
};
