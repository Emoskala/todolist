const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");

require("dotenv").config({ path: "../../../.env" });
const JWT_AT_SECRET_KEY = process.env.JWT_AT_SECRET_KEY;
const JWT_RT_SECRET_KEY = process.env.JWT_RT_SECRET_KEY;

const generateAccessToken = (username) => {
    return jwt.sign({ username }, JWT_AT_SECRET_KEY, { expiresIn: "15m" });
};

const generateRefreshToken = (username) => {
    return jwt.sign({ username }, JWT_RT_SECRET_KEY, { expiresIn: "30d" });
};

const decodeToken = (token) => {
    return jwt_decode(token);
};

module.exports = { generateRefreshToken, generateAccessToken, decodeToken };
