const Router = require("express").Router();
const { createTask, editTask, getTasks, deleteTask } = require("./task.model");
const { success } = require("../../responseApi");

Router.get("/get-tasks", async (req, res, next) => {
    const { username } = req.query;
    try {
        const tasks = await getTasks(username);
        res.send(success(200, tasks, "OK"));
    } catch (err) {
        next(err);
    }
});

Router.patch("/edit-task", async (req, res, next) => {
    const { content, taskId, username } = req.body;

    try {
        await editTask(content, taskId);
        const tasks = await getTasks(username);
        res.send(success(200, tasks, "OK"));
    } catch (err) {
        next(err);
    }
});

Router.delete("/delete-task", async (req, res, next) => {
    const { taskId, username } = req.body;

    try {
        await deleteTask(taskId);
        const tasks = await getTasks(username);
        res.send(success(200, tasks, "OK"));
    } catch (err) {
        next(err);
    }
});

Router.post("/create-task", async (req, res, next) => {
    const { content, username } = req.body;

    try {
        await createTask(content, username);
        const tasks = await getTasks(username);
        res.send(success(200, tasks, "OK"));
    } catch (err) {
        next(err);
    }
});

module.exports = Router;
