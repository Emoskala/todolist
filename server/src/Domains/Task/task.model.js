const { CustomError } = require("../../CustomError");
const db = require("../../db").getDB();

/**
 * creates task in DB
 */
const createTask = (content, username) => {
    return new Promise((resolve, reject) => {
        db.query(`INSERT INTO task (content, username) VALUES ('${content}', '${username}')`, (err, res, fields) => {
            if (err) reject(new CustomError("Something went wrong!", 500));
            else resolve(res);
        });
    });
};

/**
 * edits task in DB
 */
const editTask = (content, taskId) => {
    return new Promise((resolve, reject) => {
        db.query(`UPDATE task SET content = '${content}' WHERE id=${taskId}`, (err, res, fields) => {
            if (err) reject(new CustomError(("Something went wrong!", 500)));
            else resolve(res);
        });
    });
};

/**
 * deletes task in DB
 */
const deleteTask = (taskId) => {
    return new Promise((resolve, reject) => {
        db.query(`DELETE FROM task WHERE id=${taskId}`, (err, res, fields) => {
            if (err) reject(new CustomError(("Something went wrong!", 500)));
            else resolve(res);
        });
    });
};

/**
 * Selects all user tasks from DB
 */
const getTasks = (username) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT content, id FROM task WHERE username = '${username}'`, (err, res, fields) => {
            if (err) reject(new CustomError(("Something went wrong!", 500)));
            else resolve(res);
        });
    });
};

module.exports = { createTask, editTask, getTasks, deleteTask };
