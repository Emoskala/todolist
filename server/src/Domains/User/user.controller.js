const Router = require("express").Router();
const { registerUser, authenticateUser, refreshAccessToken } = require("./user.service");
const { success } = require("../../responseApi");

Router.post("/register", async (req, res, next) => {
    const { username, password } = req.body;

    try {
        const results = await registerUser(username, password);
        res.send(success(200, results, "OK"));
    } catch (err) {
        next(err);
    }
});

Router.post("/sign-in", async (req, res, next) => {
    const { username, password } = req.body;

    try {
        const results = await authenticateUser(username, password);
        res.send(success(200, results, "OK"));
    } catch (err) {
        next(err);
    }
});

Router.post("/refresh-access-token", async (req, res, next) => {
    const { refreshToken } = req.body;

    try {
        const results = await refreshAccessToken(refreshToken);
        results.refreshToken = null;
        res.send(success(200, results, "OK"));
    } catch (err) {
        next(err);
    }
});

module.exports = Router;
