const { CustomError } = require("../../CustomError");
const db = require("../../db").getDB();

/**
 * Returns user data from DB.
 */
const getUser = (username) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT id, username, password FROM user WHERE username = '${username}'`, (err, res, fields) => {
            if (err) {
                reject(new CustomError("Something went wrong!", 500));
            } else {
                resolve(res[0]);
            }
        });
    });
};

/**
 * Saves user in DB.
 */
const saveUser = (username, password) => {
    return new Promise((resolve, reject) => {
        db.query(`INSERT INTO user (username, password) VALUES ( '${username}', '${password}')`, (err, res, fields) => {
            if (err) {
                reject(new CustomError("Something went wrong!", 500));
            } else {
                resolve(res);
            }
        });
    });
};

module.exports = { getUser, saveUser };
