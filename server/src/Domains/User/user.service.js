const { generateAccessToken, generateRefreshToken } = require("../Token/token.service");
const { decodeToken } = require("../Token/token.service.js");
const { getUser, saveUser } = require("./user.model");
const { CustomError } = require("../../CustomError");

/**
 * Creates a user
 */
const registerUser = async (username, password) => {
    try {
        const user = await getUser(username);

        if (!user) {
            await saveUser(username, password);
        } else {
            throw new CustomError("User with given username already exists!", 409);
        }

        return authenticateUser(username, password);
    } catch (err) {
        throw err;
    }
};

/**
 * authenticates user
 */
const authenticateUser = async (username, password) => {
    try {
        const user = await getUser(username);
        if (!user) throw new CustomError("User with given username does not exist!", 404);

        const passwordMatch = user.password === password;

        if (passwordMatch) {
            return {
                username: user.username,
                accessToken: generateAccessToken(username),
                refreshToken: generateRefreshToken(password),
            };
        } else {
            throw new CustomError("Wrong password!", 401);
        }
    } catch (err) {
        throw err;
    }
};

/**
 * refreshes access token and authenticates user.
 */
const refreshAccessToken = async (refreshToken) => {
    try {
        const decodedToken = decodeToken(refreshToken);
        if (decodedToken.exp * 1000 <= Date.now()) {
            throw new CustomError("Invalid Token! You have to sign in!", 401);
        }
        const user = await getUser(decodedToken.username);
        return authenticateUser(user?.username, user?.password);
    } catch (err) {
        throw err;
    }
};

module.exports = { registerUser, authenticateUser, refreshAccessToken };
