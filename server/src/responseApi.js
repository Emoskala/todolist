/**
 * Creates a response object at success.
 */
const success = (code, results, message) => {
    return {
        error: false,
        message,
        code,
        results,
    };
};

/**
 * Creates a response object at error.
 */
const error = (status, message) => {
    return {
        error: true,
        message,
        status,
    };
};

module.exports = { success, error };
